package com.infosys.ewi_tracker.exception;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
@Setter @Getter
@AllArgsConstructor
public class TrackerException extends Exception {

	private static final long serialVersionUID = 1L;
	private Object errorResponse;
	private HttpStatus httpStatus;

}
