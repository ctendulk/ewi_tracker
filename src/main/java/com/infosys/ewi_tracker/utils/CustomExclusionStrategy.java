package com.infosys.ewi_tracker.utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.infosys.ewi_tracker.bo.EmployeeDetails;

public class CustomExclusionStrategy implements ExclusionStrategy {

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {

		return false;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return (f.getDeclaringClass()==EmployeeDetails.class && f.getName().equalsIgnoreCase("empStatus"));
	}

}
