package com.infosys.ewi_tracker.utils;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.infosys.ewi_tracker.bo.EmployeeDetails;
import com.infosys.ewi_tracker.bo.EmployeeStatus;


public class ExcelUtils {
public static XSSFWorkbook setHeader(XSSFWorkbook workbook,String sheetName) {
	XSSFSheet sheet=workbook.getSheet(sheetName);
	
	Row header=sheet.createRow(0);
	header.createCell(0).setCellValue("Emp Name");
	header.createCell(1).setCellValue("Emp No");
	header.createCell(2).setCellValue("Emp DU");
	header.createCell(3).setCellValue("Geo");
	header.createCell(4).setCellValue("Joining Date");
	header.createCell(5).setCellValue("Role Capability");
	header.createCell(6).setCellValue("DM Name for Emp DU");
	header.createCell(7).setCellValue("Onsite\\" + "Offshore");
	header.createCell(8).setCellValue("Reporting Manager");
	header.createCell(9).setCellValue("Job Band");
	header.createCell(10).setCellValue("CRR");
	header.createCell(11).setCellValue("VISA status");
	header.createCell(12).setCellValue("EWI Current Status");
	header.createCell(13).setCellValue("EWI Previous Status");
	header.createCell(14).setCellValue("Possibility of Resign\\Release");
	header.createCell(15).setCellValue("Reasons");
	header.createCell(16).setCellValue("Primary Reason");
	header.createCell(17).setCellValue("Retention Plan");
	header.createCell(18).setCellValue("AI owner");
	header.createCell(19).setCellValue("Esep Initiated");
	header.createCell(20).setCellValue("Comments");
	header.createCell(21).setCellValue("Last leave date");
	header.createCell(22).setCellValue("Reason for leave");
	header.createCell(23).setCellValue("PMO classification");

	return  workbook;
}
public static XSSFWorkbook getformattedFile(XSSFWorkbook workbook,String sheetName) {
	XSSFSheet sheet = workbook.getSheet(sheetName);
	XSSFCellStyle borderCellStyle=workbook.createCellStyle();
	XSSFCellStyle greenCellStyle=workbook.createCellStyle();
	XSSFCellStyle redCellStyle=workbook.createCellStyle();
	XSSFCellStyle blackCellStyle=workbook.createCellStyle();
	XSSFCellStyle yellowCellStyle=workbook.createCellStyle();
	XSSFCellStyle headerCellStyle=workbook.createCellStyle();
	headerCellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
	headerCellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
	headerCellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
	headerCellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
	headerCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	headerCellStyle.setFillForegroundColor(new XSSFColor(new Color(0, 50, 150)));
	XSSFFont headerFont = workbook.createFont();
	headerFont.setColor(IndexedColors.WHITE.getIndex());
	headerFont.setFontHeightInPoints((short)10);
	headerFont.setBold(true);
	headerCellStyle.setFont(headerFont);
	
	XSSFFont font = workbook.createFont();
	font.setColor(IndexedColors.BLACK.getIndex());
	font.setFontHeightInPoints((short)10);
	
	borderCellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
	borderCellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
	borderCellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
	borderCellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
	borderCellStyle.setFont(font);
	
	greenCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	greenCellStyle.setFillForegroundColor(IndexedColors.GREEN.index);
	greenCellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
	greenCellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
	greenCellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
	greenCellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);

	greenCellStyle.setFont(font);
	
	redCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	redCellStyle.setFillForegroundColor(IndexedColors.RED.index);
	redCellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
	redCellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
	redCellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
	redCellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
	redCellStyle.setFont(font);
	
	blackCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	blackCellStyle.setFillForegroundColor(IndexedColors.BLACK.index);
	blackCellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
	blackCellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
	blackCellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
	blackCellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
	blackCellStyle.setFont(font);
	
	yellowCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	yellowCellStyle.setFillForegroundColor(IndexedColors.YELLOW.index);
	yellowCellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
	yellowCellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
	yellowCellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
	yellowCellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
	yellowCellStyle.setFont(font);
	
	int lastrowNo = sheet.getLastRowNum();
	Row headerRow=sheet.getRow(0);
	for(int j=0;j<24;j++) {
		headerRow.getCell(j).setCellStyle(headerCellStyle);
		sheet.autoSizeColumn(j);
	}
	for(int i=1;i<=lastrowNo;i++) {
		Row row=sheet.getRow(i);
	if(null==row.getCell(0))
		row.createCell(0).setCellStyle(borderCellStyle);
	else
	row.getCell(0).setCellStyle(borderCellStyle);
	if(null==row.getCell(1))
		row.createCell(1).setCellStyle(borderCellStyle);
	else
	row.getCell(1).setCellStyle(borderCellStyle);
	if(null==row.getCell(2))
		row.createCell(2).setCellStyle(borderCellStyle);
	else
	row.getCell(2).setCellStyle(borderCellStyle);
	if(null==row.getCell(3))
		row.createCell(3).setCellStyle(borderCellStyle);
	else
	row.getCell(3).setCellStyle(borderCellStyle);
	if(null==row.getCell(4))
		row.createCell(4).setCellStyle(borderCellStyle);
	else
	row.getCell(4).setCellStyle(borderCellStyle);
	if(null==row.getCell(5))
		row.createCell(5).setCellStyle(borderCellStyle);
	else
	row.getCell(5).setCellStyle(borderCellStyle);
	if(null==row.getCell(6))
		row.createCell(6).setCellStyle(borderCellStyle);
	else
	row.getCell(6).setCellStyle(borderCellStyle);
	if(null==row.getCell(7))
		row.createCell(7).setCellStyle(borderCellStyle);
	else
	row.getCell(7).setCellStyle(borderCellStyle);
	if(null==row.getCell(8))
		row.createCell(8).setCellStyle(borderCellStyle);
	else
	row.getCell(8).setCellStyle(borderCellStyle);
	if(null==row.getCell(9))
		row.createCell(9).setCellStyle(borderCellStyle);
	else
	row.getCell(9).setCellStyle(borderCellStyle);
	if(null==row.getCell(10))
		row.createCell(10).setCellStyle(borderCellStyle);
	else
	row.getCell(10).setCellStyle(borderCellStyle);
	if(null==row.getCell(11))
		row.createCell(11).setCellStyle(borderCellStyle);
	else
	row.getCell(11).setCellStyle(borderCellStyle);
	if(null==row.getCell(12)) {
		row.createCell(12).setCellStyle(borderCellStyle);
	}
	else {
		if(row.getCell(12).getCellType()==0) {
		switch((int)row.getCell(12).getNumericCellValue()) {
		case 0 :	row.getCell(12).setCellStyle(blackCellStyle);
		break;
		case 1 :	row.getCell(12).setCellStyle(redCellStyle);
		break;
		case 2 :	row.getCell(12).setCellStyle(yellowCellStyle);
		break;
		case 3 :	row.getCell(12).setCellStyle(greenCellStyle);
		break;
		default : 	row.getCell(12).setCellStyle(borderCellStyle);
		}
		
	}
		else {
			row.getCell(12).setCellStyle(borderCellStyle);
		}
		}
	if(null==row.getCell(13)) {
		row.createCell(13).setCellStyle(borderCellStyle);
		}
	else
	{
		if(row.getCell(13).getCellType()==0) {
			switch((int)row.getCell(13).getNumericCellValue()) {
			case 0 :	row.getCell(13).setCellStyle(blackCellStyle);
			break;
			case 1 :	row.getCell(13).setCellStyle(redCellStyle);
			break;
			case 2 :	row.getCell(13).setCellStyle(yellowCellStyle);
			break;
			case 3 :	row.getCell(13).setCellStyle(greenCellStyle);
			break;
			default : 	row.getCell(13).setCellStyle(borderCellStyle);
			}

		}
		else {
			row.getCell(13).setCellStyle(borderCellStyle);
		}
	}
	if(null==row.getCell(14))
		row.createCell(14).setCellStyle(borderCellStyle);
	else
	row.getCell(14).setCellStyle(borderCellStyle);
	if(null==row.getCell(15))
		row.createCell(15).setCellStyle(borderCellStyle);
	else
	row.getCell(15).setCellStyle(borderCellStyle);
	if(null==row.getCell(16))
		row.createCell(16).setCellStyle(borderCellStyle);
	else
	row.getCell(16).setCellStyle(borderCellStyle);
	if(null==row.getCell(17))
		row.createCell(17).setCellStyle(borderCellStyle);
	else
	row.getCell(17).setCellStyle(borderCellStyle);
	if(null==row.getCell(18))
		row.createCell(18).setCellStyle(borderCellStyle);
	else
	row.getCell(18).setCellStyle(borderCellStyle);
	if(null==row.getCell(19))
		row.createCell(19).setCellStyle(borderCellStyle);
	else
	row.getCell(19).setCellStyle(borderCellStyle);
	if(null==row.getCell(20))
		row.createCell(20).setCellStyle(borderCellStyle);
	else
	row.getCell(20).setCellStyle(borderCellStyle);
	if(null==row.getCell(21))
		row.createCell(21).setCellStyle(borderCellStyle);
	else
	row.getCell(21).setCellStyle(borderCellStyle);
	if(null==row.getCell(22))
		row.createCell(22).setCellStyle(borderCellStyle);
	else
	row.getCell(22).setCellStyle(borderCellStyle);
	if(null==row.getCell(23))
		row.createCell(23).setCellStyle(borderCellStyle);
	else
	row.getCell(23).setCellStyle(borderCellStyle);
	}
	return workbook;
}
public static XSSFWorkbook writeToExcel(XSSFWorkbook workbook,List<EmployeeDetails> employeeDetailList,String sheetName) {
	EmployeeStatus employeeStatus=new EmployeeStatus();
	int rowNo=1;
	SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
	String prev_ewi_status=null;
	int prevMonth=Calendar.getInstance().get(Calendar.MONTH);
	int currentYear=Calendar.getInstance().get(Calendar.YEAR);
	int currentMonth=-1;
	if(prevMonth==0) {
		  prevMonth=12;
		  currentMonth=1;
		}
		else {
		currentMonth=prevMonth+1;
		}

	XSSFSheet sheet=workbook.getSheet(sheetName);
	for (EmployeeDetails employee : employeeDetailList) {
		List<EmployeeStatus> status=employee.getEmpStatus();
		Row	row=sheet.createRow(rowNo);
		if(null!=employee.getEmp_name()) 
		row.createCell(0).setCellValue(employee.getEmp_name());
		if(0!=employee.getEmp_id()) 
		row.createCell(1).setCellValue(employee.getEmp_id());
		if(null!=employee.getEmp_du()) 
		row.createCell(2).setCellValue(employee.getEmp_du());
		if(null!=employee.getGeo()) 
		row.createCell(3).setCellValue(employee.getGeo());
		if(null!=employee.getJoining_date())
		row.createCell(4).setCellValue(dateFormat.format(employee.getJoining_date()));
		if(null!=employee.getRole_capability())
		row.createCell(5).setCellValue(employee.getRole_capability());
		if(null!=employee.getDm_name_for_emp_du())
		row.createCell(6).setCellValue(employee.getDm_name_for_emp_du());
		if(null!=employee.getOnsite_offshore())
		row.createCell(7).setCellValue(employee.getOnsite_offshore());
		if(null!=employee.getReporting_manager())
		row.createCell(8).setCellValue(employee.getReporting_manager());
		if(0!=employee.getJob_band())
		row.createCell(9).setCellValue(employee.getJob_band());
		if(0!=employee.getCrr())
		row.createCell(10).setCellValue(employee.getCrr());
		if(null!=employee.getVisa_status())
		row.createCell(11).setCellValue(employee.getVisa_status());
		if(null!=employee.getAi_owner())
		row.createCell(18).setCellValue(employee.getAi_owner());
		if(null!=employee.getEsep_init())
		row.createCell(19).setCellValue(employee.getEsep_init());
		if(null!=employee.getComments())
		row.createCell(20).setCellValue(employee.getComments());
		if(null!=employee.getLast_leave_date()) {
			row.createCell(21).setCellValue(dateFormat.format(employee.getLast_leave_date()));
		}
		if(null!=employee.getReason_for_leave()) {
			row.createCell(22).setCellValue(employee.getReason_for_leave());
		}
		for (EmployeeStatus employeeStatuses : status) {
			if(null!=employeeStatuses) {
				System.out.println(employeeStatuses);
			if(employeeStatuses.getMonth()==currentMonth && employeeStatuses.getYear()==currentYear)
				employeeStatus=employeeStatuses;

			if(employeeStatuses.getMonth()==prevMonth && null!=employeeStatuses.getEwi_status()) {
				prev_ewi_status=employeeStatuses.getEwi_status();
				System.out.println("Previous Status "+prev_ewi_status+ "month "+employeeStatus.getMonth());
			}
			}
		}
		if(null!=prev_ewi_status) {
			switch(prev_ewi_status) {
			case "0" : row.createCell(13).setCellType(Cell.CELL_TYPE_NUMERIC);
			row.createCell(13).setCellValue(0);
			break;
			case "1" : row.createCell(13).setCellType(Cell.CELL_TYPE_NUMERIC);
			row.createCell(13).setCellValue(1);
			break;
			case "2" : row.createCell(13).setCellType(Cell.CELL_TYPE_NUMERIC);
			row.createCell(13).setCellValue(2);
			break;
			case "3" : row.createCell(13).setCellType(Cell.CELL_TYPE_NUMERIC);
			row.createCell(13).setCellValue(3);
			break;
			default : 
				row.createCell(13).setCellType(Cell.CELL_TYPE_STRING);
				row.createCell(13).setCellValue(prev_ewi_status);
			}

		}
		else {
			row.createCell(13).setCellValue("");
		}
		if(null!=employeeStatus) {
			if(null!=employeeStatus.getEwi_status()) {

				switch(employeeStatus.getEwi_status()) {
				case "0" : row.createCell(12).setCellType(Cell.CELL_TYPE_NUMERIC);
				row.createCell(12).setCellValue(0);
				break;
				case "1" : row.createCell(12).setCellType(Cell.CELL_TYPE_NUMERIC);
				row.createCell(12).setCellValue(1);
				break;
				case "2" : row.createCell(12).setCellType(Cell.CELL_TYPE_NUMERIC);
				row.createCell(12).setCellValue(2);
				break;
				case "3" : row.createCell(12).setCellType(Cell.CELL_TYPE_NUMERIC);
				row.createCell(12).setCellValue(3);
				break;
				default : 
					row.createCell(12).setCellType(Cell.CELL_TYPE_STRING);
					row.createCell(12).setCellValue(employeeStatus.getEwi_status());
				}
			}
		

			if(null!=employeeStatus.getPossibility_of_resign_release() && !"".equalsIgnoreCase(employeeStatus.getPossibility_of_resign_release()))
				row.createCell(14).setCellValue((int)Double.parseDouble(employeeStatus.getPossibility_of_resign_release()));
			else
				row.createCell(14).setCellValue("");
			if(null!=employeeStatus.getReason())
				row.createCell(15).setCellValue(employeeStatus.getReason());
			if(null!=employeeStatus.getPrimary_reason())
				row.createCell(16).setCellValue(employeeStatus.getPrimary_reason());
			if(null!=employeeStatus.getRetention_plan())
				row.createCell(17).setCellValue(employeeStatus.getRetention_plan());
			if(null!=employeeStatus.getPmo_classification())
				row.createCell(23).setCellValue(employeeStatus.getPmo_classification());
		}
			rowNo++;
			status=null;
		}	

	return workbook;
	}
	
public static List<EmployeeDetails> readFromExcel(XSSFSheet sheet,CellStyle cellStyle){
	EmployeeDetails details=null;
	EmployeeStatus status=null;
	List<EmployeeDetails> listEmployees=new ArrayList<EmployeeDetails>();
	List<EmployeeStatus> listEmpStatus=null;
	SimpleDateFormat sdf= new SimpleDateFormat("MM/dd/yyyy");
	Calendar calendar=Calendar.getInstance();
	int lastRow=sheet.getLastRowNum();
	for(int row=1;row<lastRow+1;row++) {
		status=new EmployeeStatus();
		details=new EmployeeDetails();
		listEmpStatus=new ArrayList<EmployeeStatus>();

		details.setEmp_name(sheet.getRow(row).getCell(0).getStringCellValue());
		details.setEmp_id((int)sheet.getRow(row).getCell(1).getNumericCellValue());
		details.setEmp_du(sheet.getRow(row).getCell(2).getStringCellValue());
		details.setGeo(sheet.getRow(row).getCell(3).getStringCellValue());
		details.setRole_capability(sheet.getRow(row).getCell(5).getStringCellValue());
		details.setDm_name_for_emp_du(sheet.getRow(row).getCell(6).getStringCellValue());
		details.setOnsite_offshore(sheet.getRow(row).getCell(7).getStringCellValue());
		details.setReporting_manager(sheet.getRow(row).getCell(8).getStringCellValue());
		sheet.getRow(row).getCell(9).setCellType(Cell.CELL_TYPE_NUMERIC);
		details.setJob_band((int)sheet.getRow(row).getCell(9).getNumericCellValue());
		details.setCrr((int)sheet.getRow(row).getCell(10).getNumericCellValue());
		details.setVisa_status(sheet.getRow(row).getCell(11).getStringCellValue());
		sheet.getRow(row).getCell(13).setCellType(Cell.CELL_TYPE_STRING);
		status.setEwi_status(sheet.getRow(row).getCell(13).getStringCellValue());
		status.setReason(sheet.getRow(row).getCell(15).getStringCellValue());
		status.setPrimary_reason(sheet.getRow(row).getCell(16).getStringCellValue());
		status.setRetention_plan(sheet.getRow(row).getCell(17).getStringCellValue());
		details.setAi_owner(sheet.getRow(row).getCell(18).getStringCellValue());
		details.setEsep_init(sheet.getRow(row).getCell(19).getStringCellValue());
		details.setComments(sheet.getRow(row).getCell(20).getStringCellValue());
		details.setReason_for_leave(sheet.getRow(row).getCell(22).getStringCellValue());
		switch(sheet.getRow(row).getCell(14).getCellType()) {
		case Cell.CELL_TYPE_STRING : status.setPossibility_of_resign_release("");
		break;
		case Cell.CELL_TYPE_NUMERIC : status.setPossibility_of_resign_release(String.valueOf(sheet.getRow(row).getCell(14).getNumericCellValue()));
		}
		
		status.setPmo_classification(sheet.getRow(row).getCell(23).getStringCellValue());
		status.setEmp_id((int)sheet.getRow(row).getCell(1).getNumericCellValue());
		status.setMonth(calendar.get(Calendar.MONTH)+1);
		status.setYear(calendar.get(Calendar.YEAR));
		sheet.getRow(row).getCell(4).setCellStyle(cellStyle);
		sheet.getRow(row).getCell(20).setCellStyle(cellStyle);
		switch(sheet.getRow(row).getCell(4).getCellType()) {
		case Cell.CELL_TYPE_STRING : details.setJoining_date(null);
		break;
		case Cell.CELL_TYPE_NUMERIC : details.setJoining_date(sheet.getRow(row).getCell(4).getDateCellValue());
		}
		switch(sheet.getRow(row).getCell(20).getCellType()) {
		case Cell.CELL_TYPE_STRING : details.setLast_leave_date(null);
		break;
		case Cell.CELL_TYPE_NUMERIC : details.setLast_leave_date(sheet.getRow(row).getCell(20).getDateCellValue());
		}

		listEmpStatus.add(status);
		details.setEmpStatus(listEmpStatus);
		listEmployees.add(details);
		status=null;
		details=null;
		listEmpStatus=null;
	}
	return listEmployees;
}
public static Map<Integer, String> getPreviousStatus(XSSFSheet sheet) {
	
	Map<Integer, String> previousStatuses=new HashMap<>();
	
	for(int row=1;row<=sheet.getLastRowNum();row++) {
		sheet.getRow(row).getCell(12).setCellType(Cell.CELL_TYPE_STRING);
		previousStatuses.put((int)sheet.getRow(row).getCell(1).getNumericCellValue(), sheet.getRow(row).getCell(12).getStringCellValue());
	}
	return previousStatuses;
}
}
