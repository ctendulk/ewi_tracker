package com.infosys.ewi_tracker.utils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.infosys.ewi_tracker.bo.EmployeeDetails;
import com.infosys.ewi_tracker.bo.EmployeeStatus;

public class EmployeeDetailsDeserializer implements JsonDeserializer<EmployeeDetails> {

	@Override
	public EmployeeDetails deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
		EmployeeDetails details=new EmployeeDetails();
		EmployeeStatus status=new EmployeeStatus();
		List<EmployeeStatus> listStatus=new ArrayList<EmployeeStatus>();
		Calendar calendar=Calendar.getInstance();
		JsonObject jsonObject=element.getAsJsonObject();
		SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
		details.setEmp_name(jsonObject.get("emp_name").getAsString());
		details.setEmp_id(Integer.parseInt(jsonObject.get("emp_id").getAsString()));
		if(jsonObject.has("dm_name_for_emp_du"))
			details.setDm_name_for_emp_du(jsonObject.get("dm_name_for_emp_du").getAsString());
		else
			details.setDm_name_for_emp_du("");
		
		if(jsonObject.has("onsite_offshore"))
			details.setOnsite_offshore(jsonObject.get("onsite_offshore").getAsString());
		else
			details.setOnsite_offshore("");
		
		if(jsonObject.has("emp_du"))
			details.setEmp_du(jsonObject.get("emp_du").getAsString());
		else
			details.setEmp_du("");
		
		if(jsonObject.has("reporting_manager"))
			details.setReporting_manager(jsonObject.get("reporting_manager").getAsString());
		else
			details.setReporting_manager("");
		
		if(jsonObject.has("visa_status"))

			details.setVisa_status(jsonObject.get("visa_status").getAsString());
		else
			details.setVisa_status("");
		
		if(jsonObject.has("role_capability"))
			details.setRole_capability(jsonObject.get("role_capability").getAsString());
		else
			details.setRole_capability("");
		
		if(jsonObject.has("ai_owner"))
			details.setAi_owner(jsonObject.get("ai_owner").getAsString());
		else
			details.setAi_owner("");
		
		if(jsonObject.has("esep_init"))
			details.setEsep_init(jsonObject.get("esep_init").getAsString());
		else
			details.setEsep_init("");
		
		if(jsonObject.has("comments"))
			details.setComments(jsonObject.get("comments").getAsString());
		else
			details.setComments("");
		
		if(jsonObject.has("reason_for_leave"))
			details.setReason_for_leave(jsonObject.get("reason_for_leave").getAsString());
		else
			details.setReason_for_leave("");
		
		if(jsonObject.has("geo"))
			details.setGeo(jsonObject.get("geo").getAsString());
		else
			details.setGeo("");
		try {
			if(jsonObject.has("last_leave_date")) {
			String strlastleavedate=jsonObject.get("last_leave_date").getAsString();
			if(null!=strlastleavedate && !strlastleavedate.trim().isEmpty()) {
				details.setLast_leave_date(sdf.parse(strlastleavedate));
			}
			}
			else {
				details.setLast_leave_date(null);
			}
			if(jsonObject.has("joining_date")) {
			String strjoiningdate=jsonObject.get("joining_date").getAsString();
			if(null!=strjoiningdate && !strjoiningdate.trim().isEmpty()) {
				details.setJoining_date(sdf.parse(strjoiningdate));
			}
			}
			else {
				details.setJoining_date(null);
			}
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(!jsonObject.has("job_band") || jsonObject.get("job_band").getAsString().trim().isEmpty()) {
			details.setJob_band(0);
		}
		else {
			details.setJob_band(Integer.parseInt(jsonObject.get("job_band").getAsString()));
		}
		if(!jsonObject.has("crr") || jsonObject.get("crr").getAsString().trim().isEmpty()) {
			details.setCrr(0);
		}else {
			details.setCrr(Integer.parseInt(jsonObject.get("crr").getAsString()));
		}
		status.setEmp_id(Integer.parseInt(jsonObject.get("emp_id").getAsString()));
		if(jsonObject.has("pmo_classification"))
			status.setPmo_classification(jsonObject.get("pmo_classification").getAsString());
		else
			status.setPmo_classification("");
		if(jsonObject.has("possibility_of_resign_release"))
			status.setPossibility_of_resign_release(jsonObject.get("possibility_of_resign_release").getAsString());
		else
			status.setPossibility_of_resign_release("");
		if(jsonObject.has("reason"))
			status.setReason(jsonObject.get("reason").getAsString());
		else
			status.setReason("");
		if(jsonObject.has("ewi_status"))
			status.setEwi_status(jsonObject.get("ewi_status").getAsString());
		else
			status.setEwi_status("3");
		if(jsonObject.has("primary_reason"))
			status.setPrimary_reason(jsonObject.get("primary_reason").getAsString());
		else
			status.setPrimary_reason("");
		if(jsonObject.has("retention_plan"))
			status.setRetention_plan(jsonObject.get("retention_plan").getAsString());
		else
			status.setRetention_plan("");
		status.setMonth(calendar.get(Calendar.MONTH)+1);
		status.setYear(calendar.get(Calendar.YEAR));

		listStatus.add(status);
		details.setEmpStatus(listStatus);
		return details;
	}

}
