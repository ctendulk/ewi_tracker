package com.infosys.ewi_tracker.utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.infosys.ewi_tracker.bo.EmployeeStatus;

public class CustomExclusionEmployeeStatus implements ExclusionStrategy {

	@Override
	public boolean shouldSkipClass(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		// TODO Auto-generated method stub
		return (f.getDeclaringClass()==EmployeeStatus.class && f.getName().equalsIgnoreCase("emp_id"));
	}

}
