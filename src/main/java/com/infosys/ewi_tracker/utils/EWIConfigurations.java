package com.infosys.ewi_tracker.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter @Setter
public class EWIConfigurations {
	@Value("${filePath}")
	private String filePath;
	@Value("${fileName}")
	private String fileName;
	@Value("${sheetName}")
	private String sheetName;
}
