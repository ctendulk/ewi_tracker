package com.infosys.ewi_tracker.service;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.infosys.ewi_tracker.bo.EmployeeDetails;
import com.infosys.ewi_tracker.bo.EmployeeStatus;
import com.infosys.ewi_tracker.repository.EmployeeRepository;
import com.infosys.ewi_tracker.utils.ExcelUtils;
import com.infosys.ewi_tracker.utils.CustomExclusionEmployeeStatus;
import com.infosys.ewi_tracker.utils.CustomExclusionStrategy;
import com.infosys.ewi_tracker.utils.EWIConfigurations;
import com.infosys.ewi_tracker.utils.EmployeeDetailsDeserializer;

@Component
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	EWIConfigurations configurations;
	
	private static int status=0;
	@Override
	public String getAllEmployees() {
		String json="[";
		int prevMonth=Calendar.getInstance().get(Calendar.MONTH);
		int currentYear=Calendar.getInstance().get(Calendar.YEAR);
		int currentMonth=-1;
		GsonBuilder builder=new  GsonBuilder().setExclusionStrategies(new CustomExclusionStrategy()).serializeNulls().excludeFieldsWithoutExposeAnnotation().setDateFormat(DateFormat.SHORT).setPrettyPrinting();
		GsonBuilder builderEmpStatus=new GsonBuilder().setExclusionStrategies(new CustomExclusionEmployeeStatus()).excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting();
		Gson gsonStatus=builderEmpStatus.create();
		List<EmployeeDetails> details = (List<EmployeeDetails>) employeeRepository.findAll();
		Gson gson=builder.create();
		
		if(prevMonth==0) {
		  prevMonth=12;
		  currentMonth=1;
		}
		else {
		currentMonth=prevMonth+1;
		}
		Integer prev_status=0;
		EmployeeStatus currentEmployeeStatus=null;
		for (EmployeeDetails employeeDetails : details) {
			if(null!=employeeDetails.getJoining_date() || null!=employeeDetails.getLast_leave_date()) 
			{
				String localjson=gson.toJson(employeeDetails);
				JsonParser jsonParser=new JsonParser();
				JsonObject object=jsonParser.parse(localjson).getAsJsonObject();
				if(null!=employeeDetails.getJoining_date()) {
					Date joiningDate;
					try {
						if(null!=object.get("joining_date")) {
						joiningDate = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss").parse(object.get("joining_date").getAsString());
						object.addProperty("joining_date", new SimpleDateFormat("MM/dd/yyyy").format(joiningDate));
						}
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if(null!=employeeDetails.getLast_leave_date()) {
					Date lastLeaveDate;
					try {
						if(null!=object.get("last_leave_date")) {
						lastLeaveDate = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss").parse(object.get("last_leave_date").getAsString());
						object.addProperty("last_leave_date", new SimpleDateFormat("MM/dd/yyyy").format(lastLeaveDate));
						}
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
				json+=object.toString();
			}
			else {
				json+=gson.toJson(employeeDetails);
			} 
			List<EmployeeStatus> status=employeeDetails.getEmpStatus();
			for (EmployeeStatus employeeStatus : status) {
				if(employeeStatus.getMonth()==currentMonth && employeeStatus.getYear()==currentYear)
					currentEmployeeStatus=employeeStatus;
				if(employeeStatus.getMonth()==prevMonth)
					prev_status=Integer.parseInt(employeeStatus.getEwi_status());
			}
			json=json.substring(0, json.length()-1)+","+gsonStatus.toJson(currentEmployeeStatus).substring(1);
			json=json.substring(0, json.length()-1)+", \"previous_status\": \""+prev_status+"\" },";


		}
		if(json.length()>2)
		json=json.substring(0, json.length()-1)+"]";
		else
		json="[]";
		return json;
	}
	
	@Override
	public String getEmployeedetails(int empId) {
		if(0==empId) {
			status=1;
			return "{\"status\": "+status+"}";
		}
		else {
		if(employeeRepository.exists(empId)) {
		int prevMonth=Calendar.getInstance().get(Calendar.MONTH);
		int currentYear=Calendar.getInstance().get(Calendar.YEAR);
		int currentMonth=-1;
		if(prevMonth==0) {
			  prevMonth=12;
			  currentMonth=1;
			}
			else {
			currentMonth=prevMonth+1;
		}
		String json="";
		EmployeeStatus currentEmployeeStatus=null;
		GsonBuilder builderEmpDetails=new  GsonBuilder().setExclusionStrategies(new CustomExclusionStrategy()).serializeNulls().setDateFormat(DateFormat.SHORT).setPrettyPrinting();
		GsonBuilder builderEmpStatus=new GsonBuilder().setExclusionStrategies(new CustomExclusionEmployeeStatus()).serializeNulls().setPrettyPrinting();
		Gson gsonEmpDetails=builderEmpDetails.create();
		Gson gsonStatus=builderEmpStatus.create();
		EmployeeDetails employeeDetails = employeeRepository.findOne(empId);
		Integer prev_status=0;
		if(null!=employeeDetails) {
		List<EmployeeStatus> employeeStatus=employeeDetails.getEmpStatus();
		json=gsonEmpDetails.toJson(employeeDetails);
		for (EmployeeStatus employeeStatus2 : employeeStatus) {
			if(employeeStatus2.getMonth()==currentMonth && employeeStatus2.getYear()==currentYear)
				currentEmployeeStatus=employeeStatus2;
			if(employeeStatus2.getMonth()==prevMonth)
				prev_status=Integer.parseInt(employeeStatus2.getEwi_status());
		}
		json=json.substring(0, json.length()-1)+","+gsonStatus.toJson(currentEmployeeStatus).substring(1);
		json=json.substring(0, json.length()-1)+", \"previous_status\": \""+prev_status+"\" }";
		}
		return json;
		}
		else {
			status=4;
			return "{\"status\": "+status+"}";
		}
		}
	}

	@Override
	public ResponseEntity<String> deleteEmployee(int empId){
	boolean isEmployeeExist = employeeRepository.exists(empId);
	if(isEmployeeExist) {
	employeeRepository.delete(empId);
	status=0;
	return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
	}
	else {
		status=1;
		return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
	}
	}

	@Override
	public ResponseEntity<String> addEmployee(String json) {
		int prevMonth=Calendar.getInstance().get(Calendar.MONTH);
		int year=Calendar.getInstance().get(Calendar.YEAR);
		if(prevMonth==0) {
			  prevMonth=12;
			  year-=1;
		}
		Gson gson = new	GsonBuilder().registerTypeAdapter(EmployeeDetails.class, new EmployeeDetailsDeserializer()).setPrettyPrinting().create();
		JsonParser parser = new JsonParser();
		
		if(!json.contains("emp_id")) {
			status=2;
			return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
		}
		else if(!json.contains("emp_name")) {
			status=3;
			return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
		}
		else {
		JsonObject jsonObject = parser.parse(json).getAsJsonObject();
		if(!employeeRepository.exists(Integer.parseInt(jsonObject.get("emp_id").getAsString()))) {
		EmployeeDetails details=null;
		details= gson.fromJson(jsonObject, EmployeeDetails.class);
		employeeRepository.save(details);
		if(null!=jsonObject.get("previous_status") && !jsonObject.get("previous_status").getAsString().trim().isEmpty()) {
		    JsonParser jsonParser=new JsonParser();
			JsonObject object=jsonParser.parse(json).getAsJsonObject();
			String previousStatus=object.get("previous_status").getAsString();
				EmployeeStatus employeeStatus=new EmployeeStatus();
				employeeStatus.setEmp_id(Integer.parseInt(object.get("emp_id").getAsString()));
				employeeStatus.setMonth(prevMonth);
				employeeStatus.setYear(year);
				employeeStatus.setEwi_status(previousStatus);
				employeeRepository.addPreviousEmployeeStatus(employeeStatus);
		}
		status=1;
		return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
		
		}
		else {
			status=4;
			return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
		}
		}
	}	
	

	@Override
	public ResponseEntity<String> updateEmployee(String json) {
		int prevMonth=Calendar.getInstance().get(Calendar.MONTH);
		int year=Calendar.getInstance().get(Calendar.YEAR);
		if(prevMonth==0) {
			  prevMonth=12;
			  year-=1;
		}
		Gson gson = new	GsonBuilder().registerTypeAdapter(EmployeeDetails.class, new EmployeeDetailsDeserializer()).setPrettyPrinting().create();
		JsonParser parser = new JsonParser();
		if(!json.contains("emp_id")) {
			status=2;
			return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
		}
		else if(!json.contains("emp_name")) {
			status=3;
			return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
		}
		JsonObject jsonObject = parser.parse(json).getAsJsonObject();
		if(employeeRepository.exists(Integer.parseInt(jsonObject.get("emp_id").getAsString()))) {

			EmployeeDetails details=null;
			details= gson.fromJson(jsonObject, EmployeeDetails.class);
			
			if(null!=jsonObject.get("previous_status") && !jsonObject.get("previous_status").getAsString().trim().isEmpty()) {
				JsonParser jsonParser=new JsonParser();
				JsonObject object=jsonParser.parse(json).getAsJsonObject();
				String previousStatus=object.get("previous_status").getAsString();
				if(employeeRepository.IsPreviousEmployeeStatus(Integer.parseInt(jsonObject.get("emp_id").getAsString()))) {
					System.out.println("Employee Status Already Exist, updating the status");
					EmployeeStatus employeeStatus=new EmployeeStatus();
					employeeStatus.setEmp_id(Integer.parseInt(object.get("emp_id").getAsString()));
					employeeStatus.setMonth(prevMonth);
					employeeStatus.setYear(year);
					employeeStatus.setEwi_status(previousStatus);
					employeeRepository.updatePreviousEmployeeStatus(employeeStatus);

				}
				else {
					System.out.println("Employee Status Not Exist. inserting the status");
					EmployeeStatus employeeStatus=new EmployeeStatus();
					employeeStatus.setEmp_id(Integer.parseInt(object.get("emp_id").getAsString()));
					employeeStatus.setMonth(prevMonth);
					employeeStatus.setYear(year);
					employeeStatus.setEwi_status(previousStatus);
					employeeRepository.addPreviousEmployeeStatus(employeeStatus);
				}
				
			}
			employeeRepository.updateEmployee(details);
			status=0;
			return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
		}
		else {
			status=4;
			return new ResponseEntity<String>("{\"status\": "+status+"}", HttpStatus.OK);
		}
	}

	@Override
	public ResponseEntity<String> uploadExcel(MultipartFile file) {
		List<EmployeeDetails> listEmployees=new ArrayList<EmployeeDetails>();
		Map<Integer, String> previousStatuses=null;
	
		if(null!=file && !file.isEmpty()) {
			try {
				InputStream inputStream=file.getInputStream();
				XSSFWorkbook workbook=new XSSFWorkbook(inputStream);
				CellStyle cellStyle=workbook.createCellStyle();
				cellStyle.setDataFormat(workbook.getCreationHelper().createDataFormat().getFormat("MM/dd/yyyy"));
				XSSFSheet sheet = workbook.getSheet(configurations.getSheetName());
				listEmployees=ExcelUtils.readFromExcel(sheet,cellStyle);
				previousStatuses=ExcelUtils.getPreviousStatus(sheet);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			employeeRepository.save(listEmployees);
			employeeRepository.updateStatuses(previousStatuses);
			EmployeeServiceImpl.status=0;
			return new ResponseEntity<String>("{\"status\": "+EmployeeServiceImpl.status+"}",HttpStatus.OK);
		}
	return new ResponseEntity<String>("File is not present",HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> downloadExcel() throws FileNotFoundException, MalformedURLException {
		ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
	//	URL url = new URL("");
	//	File file = new File(url.getPath());
		FileOutputStream fileopStream =  new FileOutputStream(configurations.getFilePath()+configurations.getFileName());
		XSSFWorkbook workbook=null;
		List<EmployeeDetails> employeeDetailList = (List<EmployeeDetails>) employeeRepository.findAll();
		workbook=new XSSFWorkbook();
		workbook.createSheet(configurations.getSheetName());
		workbook=ExcelUtils.setHeader(workbook,configurations.getSheetName());
		workbook=ExcelUtils.writeToExcel(workbook, employeeDetailList,configurations.getSheetName());
		
		try {
			workbook=ExcelUtils.getformattedFile(workbook,configurations.getSheetName());			
			workbook.write(fileopStream);
			System.out.println("file written success");
			byteArrayOutputStream.flush();
			byteArrayOutputStream.close();
		} catch (IOException e) {
			System.out.println("Opps There is some error while writing data");
			e.printStackTrace();
			
		}
		EmployeeServiceImpl.status=0;
		return new ResponseEntity<String>("{\"status\": "+EmployeeServiceImpl.status+",\"fileName\": \"EWI_Tracker.xlsx\""+"}", HttpStatus.OK);		
	}

	@Override
	public ResponseEntity<String> deleteAllEmployee() {
		employeeRepository.deleteAll();
		EmployeeServiceImpl.status=0;
		return new ResponseEntity<String>("{\"status\": "+EmployeeServiceImpl.status+"}",HttpStatus.OK);
	}
	

}
