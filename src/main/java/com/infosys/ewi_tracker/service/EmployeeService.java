package com.infosys.ewi_tracker.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.infosys.ewi_tracker.exception.TrackerException;

public interface EmployeeService {
	String getAllEmployees() ;
	String getEmployeedetails(int empId) ;
	ResponseEntity<String> deleteEmployee(int empId);
	ResponseEntity<String> addEmployee(String json);
	ResponseEntity<String> updateEmployee(String json);
	ResponseEntity<String> uploadExcel(MultipartFile file);
	ResponseEntity<String> downloadExcel() throws FileNotFoundException, MalformedURLException;
	ResponseEntity<String> deleteAllEmployee();
	}
