package com.infosys.ewi_tracker.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name="emp_status")
@ToString
@Setter@Getter
public class EmployeeStatus {
	@Id
	@Column(name="emp_status_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int emp_status_primary;
	
	@Column(name="Month")
	@Expose(serialize=false)
	private int month;
	
	@Column(name="Year")
	@Expose(serialize=false)
	private int year;
	
	@Column(name="ewi_status")
	@Expose(serialize=true)
	private String ewi_status;
	
	@Column(name="possibility_of_resign_release")
	@Expose(serialize=true)
	private String possibility_of_resign_release;
	
	@Column(name="reason")
	@Expose(serialize=true)
	private String reason;
	
	@Column(name="primary_reason")
	@Expose(serialize=true)
	private String primary_reason;
	
	@Column(name="retention_plan")
	@Expose(serialize=true)
	private String retention_plan;
	
	@Column(name="pmo_classification")
	@Expose(serialize=true)
	private String pmo_classification;
	
	@Column()
	private int emp_id;
}
