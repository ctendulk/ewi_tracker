package com.infosys.ewi_tracker.bo;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="emp_detail")
@Getter @Setter
@ToString
public class EmployeeDetails {
	@Id
	@Column(name="emp_id")
	@Expose(serialize=true)
	private int emp_id;
	
	@Column(name="emp_name")
	@Expose(serialize=true)
	private String emp_name;

	@Column(name="role_capability")
	@Expose(serialize=true)
	private String role_capability;

	@Column(name="emp_du")
	@Expose(serialize=true)
	private String emp_du;

	@Column(name="dm_name_for_emp_du")
	@Expose(serialize=true)
	private String dm_name_for_emp_du;

	@Column(name="onsite_offshore")
	@Expose(serialize=true)
	private String onsite_offshore;

	@Column(name="reporting_manager")
	@Expose(serialize=true)
	private String reporting_manager;
	
	@Column(name="job_band")
	@Expose(serialize=true)
	private int job_band;

	@Column(name="joining_date")
	@Expose(serialize=true)
	private Date joining_date;

	@Column(name="crr")
	@Expose(serialize=true)
	private int crr;

	@Column(name="visa_status")
	@Expose(serialize=true)
	private String visa_status;
	
	@Column(name="ai_owner")
	@Expose(serialize=true)
	private String ai_owner;
	
	@Column(name="esep_init")
	@Expose(serialize=true)
	private String esep_init;
	
	@Column(name="comments")
	@Expose(serialize=true)
	private String comments;
	
	@Column(name="last_leave_date")
	@Expose(serialize=true)
	private Date last_leave_date;
	
	@Column(name="reason_for_leave")
	@Expose(serialize=true)
	private String reason_for_leave;
	
	@Column(name="geo")
	@Expose(serialize=true)
	private String geo;
	
	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.REMOVE,CascadeType.MERGE})
	@JoinColumn(name="emp_id")
	private List<EmployeeStatus> empStatus;


}
