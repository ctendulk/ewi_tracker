package com.infosys.ewi_tracker.controller;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.infosys.ewi_tracker.service.EmployeeService;

@RestController
@CrossOrigin
public class EmployeeFileController {
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value="/uploadFile",method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> uploadExcel(@RequestBody MultipartFile file) {
		return employeeService.uploadExcel(file);
	}
	
	@RequestMapping(value="/downloadFile",method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> downloadExcel() throws FileNotFoundException, MalformedURLException   {
		return employeeService.downloadExcel();
	}
}
