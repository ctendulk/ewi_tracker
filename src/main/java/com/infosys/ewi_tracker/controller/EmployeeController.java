package com.infosys.ewi_tracker.controller;


import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.infosys.ewi_tracker.exception.TrackerException;
import com.infosys.ewi_tracker.service.EmployeeService;

@RestController
@CrossOrigin
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value="/getAllEmployees",method = RequestMethod.GET)
	public @ResponseBody String getAllEmployees() throws SQLException{
		return employeeService.getAllEmployees();
	}
	
	@RequestMapping(value="/employee/{empId}",method = RequestMethod.GET)
	public @ResponseBody String getEmployeeInformation(@PathVariable int empId) throws TrackerException {
		return employeeService.getEmployeedetails(empId);
	}
	
	@RequestMapping(value="/deleteEmployee/{empId}",method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteEmployee(@PathVariable int empId) {
		return employeeService.deleteEmployee(empId);
	}
	
	@RequestMapping(value="/addEmployee",method = RequestMethod.POST)
	public ResponseEntity<String> addEmployee(@RequestBody String json) throws TrackerException {
		return employeeService.addEmployee(json);
	}
	
	@RequestMapping(value="/updateEmployee",method = RequestMethod.POST)
	public ResponseEntity<String> updateEmployee(@RequestBody String json) throws TrackerException {
		return employeeService.updateEmployee(json);
	}
	
	@RequestMapping(value="/deleteAllEmployee",method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteAllEmployee() throws TrackerException {
		return employeeService.deleteAllEmployee();
	}
}
