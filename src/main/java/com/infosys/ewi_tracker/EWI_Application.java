package com.infosys.ewi_tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages="com.infosys.ewi_tracker")
public class EWI_Application {
public static void main(String[] args) {
	SpringApplication.run(EWI_Application.class, args);
}
}
