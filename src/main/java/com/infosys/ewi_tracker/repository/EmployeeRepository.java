package com.infosys.ewi_tracker.repository;

import java.util.Map;

import org.springframework.data.repository.CrudRepository;
import com.infosys.ewi_tracker.bo.EmployeeDetails;
import com.infosys.ewi_tracker.bo.EmployeeStatus;

public interface EmployeeRepository extends CrudRepository<EmployeeDetails, Integer> {
void updateEmployee(EmployeeDetails details);
void updatePreviousEmployeeStatus(EmployeeStatus employeeStatus);
void addPreviousEmployeeStatus(EmployeeStatus employeeStatus);
boolean IsPreviousEmployeeStatus(Integer empStatusId);
void updateStatuses(Map<Integer, String> statuses);
}
