package com.infosys.ewi_tracker.repository;

import org.springframework.data.repository.CrudRepository;

import com.infosys.ewi_tracker.bo.EmployeeStatus;

public interface EmployeeStatusRepository extends CrudRepository<EmployeeStatus, Integer> {
void updateEmployeeStatus(int empStatusId);
}
