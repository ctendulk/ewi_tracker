package com.infosys.ewi_tracker.repository;


import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.infosys.ewi_tracker.bo.EmployeeDetails;
import com.infosys.ewi_tracker.bo.EmployeeStatus;

@Repository
@Transactional
public class EmployeeRepositoryImpl implements EmployeeRepository{
    @PersistenceContext
	private EntityManager em;

	@Override
	public long count() {
		
		return 0;
	}

	@Override
	public void delete(Integer empId){
		Query query =	em.createQuery("from EmployeeDetails e where e.emp_id = ?");
		Query queryDeleteEmployeedetails=em.createQuery("Delete from EmployeeDetails e where e.emp_id = ?");
		Query queryDeleteStatus =null;
		query.setParameter(1, empId);
		EmployeeDetails employeeDetails=(EmployeeDetails) query.getSingleResult();
		List<EmployeeStatus> employeeStatus=employeeDetails.getEmpStatus();
		for (EmployeeStatus employeeStatus2 : employeeStatus) {
			queryDeleteStatus=em.createQuery("delete from EmployeeStatus e where e.emp_status_primary = ?");
			queryDeleteStatus.setParameter(1, employeeStatus2.getEmp_status_primary());
			queryDeleteStatus.executeUpdate();
		}
		queryDeleteEmployeedetails.setParameter(1, empId);
		queryDeleteEmployeedetails.executeUpdate();
	}

	@Override
	public void delete(EmployeeDetails empId) {

	}

	@Override
	public void delete(Iterable<? extends EmployeeDetails> arg0) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		Query queryDeleteEmployeestatus=em.createQuery("Delete from EmployeeStatus");
		Query queryDeleteEmployeedetails=em.createQuery("Delete from EmployeeDetails");
		queryDeleteEmployeestatus.executeUpdate();
		queryDeleteEmployeedetails.executeUpdate();
	}

	@Override
	public boolean exists(Integer empId) {
		EmployeeDetails details= em.find(EmployeeDetails.class, empId);
		if(null==details)
		return false;
		else
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<EmployeeDetails> findAll() {
		// TODO Auto-generated method stub
		Query query =	em.createQuery("from EmployeeDetails");
		Iterable<EmployeeDetails> iterable= (Iterable<EmployeeDetails>)query.getResultList();
		return iterable;
	}

	@Override
	public Iterable<EmployeeDetails> findAll(Iterable<Integer> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeDetails findOne(Integer empId) {
		Query query =	em.createQuery("from EmployeeDetails e where e.emp_id = ?");
		EmployeeDetails details=null;
		query.setParameter(1, empId);
		details = (EmployeeDetails) query.getSingleResult();
		return details;
	}

	@Override
	public <S extends EmployeeDetails> S save(S arg0) {
		EmployeeDetails details= (EmployeeDetails)arg0;
		em.persist(details);
		em.flush();
		return arg0;
	}

	@Override
	public <S extends EmployeeDetails> Iterable<S> save(Iterable<S> employees) {
		List<EmployeeDetails> list=(List<EmployeeDetails>)employees;
		for (EmployeeDetails employeeDetails : list) {
			if(exists(employeeDetails.getEmp_id())) {
				System.out.println(employeeDetails.getEmp_id()+" Already Exist");
			}
			else {
				em.persist(employeeDetails);
				em.flush();
			}
		}
		
		return employees;
	}

	@Override
	@Transactional
	public void updateEmployee(EmployeeDetails details) {
		int currMonth=Calendar.getInstance().get(Calendar.MONTH)+1;
		int year=Calendar.getInstance().get(Calendar.YEAR);

		EmployeeDetails employeeDetails = em.find(EmployeeDetails.class, details.getEmp_id());
		Query query=em.createQuery("from EmployeeStatus where emp_id=?");
		query.setParameter(1, details.getEmp_id());
		employeeDetails.setAi_owner(details.getAi_owner());
		employeeDetails.setComments(details.getComments());
		employeeDetails.setCrr(details.getCrr());
		employeeDetails.setDm_name_for_emp_du(details.getDm_name_for_emp_du());
		employeeDetails.setEmp_du(details.getEmp_du());
		employeeDetails.setEmp_name(details.getEmp_name());
		List<EmployeeStatus> OldemployeeStatuses=query.getResultList();
		EmployeeStatus emplStatus=null;
		for (EmployeeStatus employeeStatus2 : OldemployeeStatuses) {
			if(employeeStatus2.getMonth()==currMonth && employeeStatus2.getYear()==year) {
				emplStatus=em.find(EmployeeStatus.class, employeeStatus2.getEmp_status_primary());
			}
		}
		if(null!=emplStatus) {
			EmployeeStatus newEmployeeStatus = details.getEmpStatus().get(0);
			emplStatus.setEwi_status(newEmployeeStatus.getEwi_status());
			emplStatus.setPmo_classification(newEmployeeStatus.getPmo_classification());
			emplStatus.setPossibility_of_resign_release(newEmployeeStatus.getPossibility_of_resign_release());
			emplStatus.setReason(newEmployeeStatus.getReason());
			emplStatus.setPrimary_reason(newEmployeeStatus.getPrimary_reason());
			emplStatus.setRetention_plan(newEmployeeStatus.getRetention_plan());
		}
		employeeDetails.setEsep_init(details.getEsep_init());
		employeeDetails.setGeo(details.getGeo());
		employeeDetails.setJob_band(details.getJob_band());
		employeeDetails.setJoining_date(details.getJoining_date());
		employeeDetails.setLast_leave_date(details.getJoining_date());
		employeeDetails.setLast_leave_date(details.getLast_leave_date());
		employeeDetails.setOnsite_offshore(details.getOnsite_offshore());
		employeeDetails.setReason_for_leave(details.getReason_for_leave());
		employeeDetails.setRole_capability(details.getRole_capability());
		employeeDetails.setReporting_manager(details.getReporting_manager());
		employeeDetails.setVisa_status(details.getVisa_status());
		em.flush();
	}

	@Override
	public void updatePreviousEmployeeStatus(EmployeeStatus employeeStatus) {
		// TODO Auto-generated method stub
		System.out.println("updatePreviousEmployeeStatus");
		Query query =	em.createQuery("update EmployeeStatus ES set ES.ewi_status=? where ES.emp_id=? AND ES.month=? AND ES.year=?,");

		query.setParameter(1,employeeStatus.getEwi_status());
		query.setParameter(2, employeeStatus.getEmp_id());
		query.setParameter(3, employeeStatus.getMonth());
		query.setParameter(4, employeeStatus.getYear());
		query.executeUpdate();
	
	}

	@Override
	public void addPreviousEmployeeStatus(EmployeeStatus employeeStatus) {
	em.persist(employeeStatus);
	em.flush();
	
	}

	@Override
	public boolean IsPreviousEmployeeStatus(Integer empId) {
		int prevMonth=Calendar.getInstance().get(Calendar.MONTH);
		int year=Calendar.getInstance().get(Calendar.YEAR);
		if(prevMonth==0) {
			  prevMonth=12;
			  year-=1;
		}

		Query query1 =	em.createQuery("from EmployeeStatus where emp_id=?");
		query1.setParameter(1, empId);
		List<EmployeeStatus> employeeStatus= query1.getResultList();
		for (EmployeeStatus employeeStatus2 : employeeStatus) {
			System.out.println("Month : "+employeeStatus2.getMonth()+" Year : "+employeeStatus2.getYear());
			if(employeeStatus2.getMonth()==prevMonth && employeeStatus2.getYear()==year)
				return true;
		}

		return false;
	}

	@Override
	public void updateStatuses(Map<Integer, String> empStatuses) {
		int prevMonth=Calendar.getInstance().get(Calendar.MONTH);
		int year=Calendar.getInstance().get(Calendar.YEAR);
		if(prevMonth==0) {
			  prevMonth=12;
			  year-=1;
		}
		Set<Entry<Integer, String>> entries = empStatuses.entrySet();
		for (Entry<Integer, String> entry : entries) {
			EmployeeStatus employeeStatus=new EmployeeStatus();
			employeeStatus.setEmp_id(entry.getKey());
			employeeStatus.setMonth(prevMonth);
			employeeStatus.setYear(year);
			employeeStatus.setEwi_status(entry.getValue());
			em.persist(employeeStatus);
			em.flush();
		}
		
		
		
		
	}
	



}
